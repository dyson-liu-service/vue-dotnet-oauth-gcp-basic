docker-compose down
Remove-Item Env:\GOOGLE_CLIENT_ID
Remove-Item Env:\GOOGLE_CLIENT_SECRET
Remove-Item Env:\GOOGLE_REDIRECT_URI
Remove-Item Env:\ALLOWED_ORIGINS
Remove-Item Env:\PORT
Remove-Item Env:\BACKEND_PROXY_URI
Remove-Item Env:\FRONTEND_PROXY_URI