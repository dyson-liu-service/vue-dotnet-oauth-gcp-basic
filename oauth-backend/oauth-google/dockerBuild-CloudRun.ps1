# Script of PowerShell 
docker build `
  --build-arg ASPNETCORE_URLS=http://+:8080 `
  --build-arg AllowedOrigins=<FRONTEND_URI> `
  --build-arg Google__ClientId=<GOOGLE_CLIENT_ID> `
  --build-arg Google__ClientSecret=<GOOGLE_CLIENT_SECRET> `
  --build-arg Google__RedirectUri=<FRONTEND_URI> `
  -t asia-east1-docker.pkg.dev/vue-dotnet-oauth/vue-dotnet-ex-docker-repository/oauth-backend-api:1.0 `
  -f Dockerfile ..

# In Linux scripts (bash/sh), 
# you should change the backtick (`) to a backslash (\) 
# for line continuation.

docker image prune -f

docker push asia-east1-docker.pkg.dev/vue-dotnet-oauth/vue-dotnet-ex-docker-repository/oauth-backend-api:1.0
