﻿using System.Text.Json.Serialization;

namespace oauth_google.Models
{
    public class ResponseBase<T> 
    {
        [JsonPropertyName("result")]
        public bool Result { get; set; } = false;

        [JsonPropertyName("message")]
        public string Message { get; set; } = string.Empty;

        [JsonPropertyName("data")]
        public T? Data { get; set; } = default;

    }
}
