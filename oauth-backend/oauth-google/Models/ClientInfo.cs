﻿using System.Text.Json.Serialization;

namespace oauth_google.Models
{
    public class ClientInfo
    {
        [JsonPropertyName("client_id")]
        public string? ClientId { get; set; }
    }
}
