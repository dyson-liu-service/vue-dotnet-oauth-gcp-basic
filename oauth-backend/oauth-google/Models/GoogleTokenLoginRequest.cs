﻿using System.Text.Json.Serialization;

namespace oauth_google.Models
{
    public class GoogleTokenLoginRequest
    {
        [JsonPropertyName("access_token")]
        public string? AccessToken { get; set; }
    }
}
