﻿using System.Text.Json.Serialization;

namespace oauth_google.Models
{
    public class GoogleAuthCodeLoginRequest
    {
        [JsonPropertyName("code")]
        public string? Code { get; set; }

        [JsonPropertyName("scope")]
        public string? Scope { get; set; }

        [JsonPropertyName("authuser")] 
        public string? Authuser { get; set; }

        [JsonPropertyName("prompt")] 
        public string? Prompt { get; set; }

    }
}
