﻿using System.Text.Json.Serialization;

namespace oauth_google.Models
{
    public class GoogleTokenInfo
    {
        [JsonPropertyName("access_token")]
        public string? AccessToken { get; set; }
    }
}
