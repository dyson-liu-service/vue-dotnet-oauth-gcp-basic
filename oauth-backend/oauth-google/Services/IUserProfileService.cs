﻿using oauth_google.Models;

namespace oauth_google.Services
{
    public interface IUserProfileService
    {
        void AddUserProfile(string id, UserProfile userProfile);
    }
}
