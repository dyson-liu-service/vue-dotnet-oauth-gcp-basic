﻿using oauth_google.Models;
using System.Collections.Concurrent;

namespace oauth_google.Services
{
    public class UserProfileService : IUserProfileService
    {
        private readonly ConcurrentDictionary<string, UserProfile> _userProfiles = new();

        public void AddUserProfile(string id, UserProfile userProfile)
        {
            _userProfiles.TryAdd(id, userProfile);
        }
    }
}
