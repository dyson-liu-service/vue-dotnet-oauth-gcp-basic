﻿using Microsoft.AspNetCore.Mvc;
using oauth_google.Models;
using oauth_google.Services;
using System.Text.Json;

namespace oauth_google.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthGoogleController : Controller
    {
        private readonly ILogger<AuthGoogleController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IUserProfileService _userProfileService;
        private static readonly string GoogleApiUrl = "https://www.googleapis.com/oauth2/v3/userinfo";
        private static readonly string GoogleTokenUrl = "https://oauth2.googleapis.com/token";

        public AuthGoogleController(
            ILogger<AuthGoogleController> logger,
            IConfiguration configuration,
            IUserProfileService userProfileService)
        {
            _logger = logger;
            _configuration = configuration;
            _userProfileService = userProfileService;
        }

        // Add this method
        [HttpGet("ping")]
        public IActionResult Ping()
        {
            _logger.LogInformation("Received ping request");
            return Ok("pong");
        }


        [HttpGet("ClientInfo")]
        public async Task<ActionResult<ResponseBase<ClientInfo>>> ClientInfo()
        {
            ResponseBase<ClientInfo> funcResponse = new()
            {
                Result = true,
                Data = new ClientInfo
                {
                    ClientId = _configuration["Google:ClientId"]
                }
            };
            return await Task.FromResult(Ok(funcResponse));
        }

        [HttpPost("TokenLogin")]
        public async Task<ActionResult<ResponseBase<UserProfile>>> TokenLogin([FromBody] GoogleTokenLoginRequest googleLoginRequest)
        {
            ResponseBase<UserProfile> funcResponse = new();

            try
            {
                // check request format
                string? accessToken = googleLoginRequest.AccessToken ?? throw new Exception();

                using HttpClient httpClient = new();
                var response = await httpClient.GetAsync(GoogleApiUrl + $"?access_token={accessToken}");

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    GoogleUserInfo? googleUserProfile = System.Text.Json.JsonSerializer.Deserialize<GoogleUserInfo>(content);

                    if (googleUserProfile != null && !string.IsNullOrWhiteSpace(googleUserProfile.Sub))
                    {
                        UserProfile? userProfile = SaveUserProfile(googleUserProfile);

                        funcResponse.Result = true;
                        funcResponse.Data = userProfile;
                        return Ok(funcResponse);
                    }
                }

                funcResponse.Message = "Invalid token.";
                return BadRequest(funcResponse);
            }
            catch (Exception)
            {
                funcResponse.Message = "Invalid or expired token.";
                return Unauthorized();
            }
        }

        [HttpPost("AuthCodeLogin")]
        public async Task<ActionResult<ResponseBase<UserProfile>>> AuthCodeLogin([FromBody] GoogleAuthCodeLoginRequest googleLoginRequest)
        {
            ResponseBase<UserProfile> funcResponse = new();

            try
            {
                // check request format
                string? authCode = googleLoginRequest.Code ?? throw new Exception();

                // Exchange authorization code for access token
                var tokenRequestBody = new Dictionary<string, string>()
                {
                    {"code", authCode},
                    {"client_id", _configuration["Google:ClientId"]},
                    {"client_secret", _configuration["Google:ClientSecret"]},
                    {"redirect_uri", _configuration["Google:RedirectUri"]},
                    {"grant_type", "authorization_code"}
                };
                _logger.LogWarning("tokenRequestBody redirect_uri: {redirect_uri}.", tokenRequestBody["redirect_uri"]);

                using HttpClient httpClient = new();
                var tokenResponse = await httpClient.PostAsync(GoogleTokenUrl, new FormUrlEncodedContent(tokenRequestBody));
                if (!tokenResponse.IsSuccessStatusCode)
                {
                    var errorContent = await tokenResponse.Content.ReadAsStringAsync();
                    _logger.LogError("Failed to exchange authorization code for access token: {StatusCode}. Response content: {ResponseContent}", tokenResponse.StatusCode, errorContent);

                    return Unauthorized();
                }

                var tokenContent = await tokenResponse.Content.ReadAsStringAsync();
                var tokenInfo = JsonSerializer.Deserialize<GoogleTokenInfo>(tokenContent);
                if (tokenInfo == null || tokenInfo.AccessToken == null)
                {
                    _logger.LogError(message: "Failed to deserialize exchange authorization code for access token");
                    return Unauthorized();
                }

                // Use access token to get user info
                var request = new HttpRequestMessage(HttpMethod.Get, GoogleApiUrl);
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", tokenInfo.AccessToken);
                var response = await httpClient.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    GoogleUserInfo? googleUserProfile = System.Text.Json.JsonSerializer.Deserialize<GoogleUserInfo>(content);

                    if (googleUserProfile != null && !string.IsNullOrWhiteSpace(googleUserProfile.Sub))
                    {
                        UserProfile? userProfile = SaveUserProfile(googleUserProfile);

                        funcResponse.Result = true;
                        funcResponse.Data = userProfile;
                        return Ok(funcResponse);
                    }
                }

                funcResponse.Message = "Invalid authorization code.";
                return BadRequest(funcResponse);

            }
            catch (Exception)
            {
                funcResponse.Message = "Invalid Authorization Code.";
                return Unauthorized();
            }
        }

        private UserProfile? SaveUserProfile(GoogleUserInfo? googleUserProfile)
        {
            if (googleUserProfile != null && googleUserProfile.Sub != null)
            {
                UserProfile userProfile = new()
                {
                    Provider = "Google",
                    Sub = googleUserProfile.Sub,
                    Name = googleUserProfile.Name,
                    Email = googleUserProfile.Email,
                    Picture = googleUserProfile.Picture,
                };
                _userProfileService.AddUserProfile(
                    id: googleUserProfile.Sub,
                    userProfile: userProfile
                    );
                return userProfile;
            }

            return null;
        }

    }
}