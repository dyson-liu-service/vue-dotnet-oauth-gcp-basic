using oauth_google.Services;

var allowSpecificOrigins = "_allowSpecificOrigins";
var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<IUserProfileService, UserProfileService>();

// Add CORS policy
var allowedOrigins = configuration["AllowedOrigins"]?.Split(',');
builder.Services.AddCors(options =>
{
    options.AddPolicy(
        name: allowSpecificOrigins,
        policy =>
        {
            if (allowedOrigins != null)
            {
                policy.WithOrigins(allowedOrigins)
                            .AllowAnyHeader()
                            .AllowAnyMethod();
            }
        });
});

var app = builder.Build();

// Get the logger.
var logger = app.Services.GetRequiredService<ILogger<Program>>();

// Log the allowed origins.
if (allowedOrigins != null)
    logger.LogInformation("Allowed Origins: {Origins}", string.Join(", ", allowedOrigins));
else
    logger.LogInformation("No allowed origins specified.");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(allowSpecificOrigins);  // UseCors must be placed after UseRouting, but before UseAuthorization.
app.UseAuthorization();
app.MapControllers();
app.Run();
