$Env:GOOGLE_CLIENT_ID="your-client-id"
$Env:GOOGLE_CLIENT_SECRET="your-client-secret"
$Env:GOOGLE_REDIRECT_URI="your-redirect-uri"
$Env:ALLOWED_ORIGINS="your-allowed-origins"
$Env:PORT="8080"
$Env:BACKEND_PROXY_URI="http://oauth-backend:80"
$Env:FRONTEND_PROXY_URI="http://oauth-frontend:80"
docker-compose up --build