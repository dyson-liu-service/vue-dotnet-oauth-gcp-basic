docker build --build-arg BUILD_MODE=cloud_run -t asia-east1-docker.pkg.dev/vue-dotnet-oauth/vue-dotnet-ex-docker-repository/oauth-frontend-app:1.0 .
docker image prune -f
docker push asia-east1-docker.pkg.dev/vue-dotnet-oauth/vue-dotnet-ex-docker-repository/oauth-frontend-app:1.0

# Testing in localhost
docker run -e PORT=8080 -p 3000:8080 -d --rm --name oauth-frontend-app asia-east1-docker.pkg.dev/vue-dotnet-oauth/vue-dotnet-ex-docker-repository/oauth-frontend-app:1.0
