import { googleAuthCodeLogin, googleTokenLogin } from 'vue3-google-login'
import { ClientInfoResponse, UserProfileResponse } from '../types/ResponseTypes'

export const googleClientInfoFetch = async (onResult: (result: ClientInfoResponse) => void): Promise<void> => {
    // const backendUrl = `http://${window.location.hostname}/api/AuthGoogle/ClientInfo`
    // const backendUrl = '/api/AuthGoogle/ClientInfo'
    console.log(`import.meta.env.VITE_BACKEND_URI: ${import.meta.env.BACKEND_URI}`)
    const backendUrl = `${import.meta.env.VITE_BACKEND_URI || ''}/api/AuthGoogle/ClientInfo`
    console.log(`fetch URL: ${backendUrl}`)
    const response = await fetch(backendUrl)
    if (response.ok) {
        const data = await response.json()
        onResult(data)
    } else {
        console.error('Failed to fetch client info:', response.statusText)
        onResult(new ClientInfoResponse())
    }
}

export const googleTokenLoginFetch = async (onResult: (result: UserProfileResponse) => void, clientId: string): Promise<void> => {
    const resGoogle = await googleTokenLogin({clientId: clientId})
    console.log("Handle the response", resGoogle)

    // const backendUrl = `http://${window.location.hostname}/api/AuthGoogle/TokenLogin`
    // const backendUrl = '/api/AuthGoogle/TokenLogin'
    const backendUrl = `${import.meta.env.VITE_BACKEND_URI || ''}/api/AuthGoogle/TokenLogin`
    console.log(`fetch URL: ${backendUrl}`)
    const response = await fetch(backendUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(resGoogle)
    })

    if (response.ok) {
        const data = await response.json()
        console.log('Server response:', data)    
        onResult(data)
    }
    else {
        onResult(new UserProfileResponse())
    }
}

export const googleAuthCodeFetch = async (onResult: (result: UserProfileResponse) => void, clientId: string): Promise<void> => {
    const resGoogle = await googleAuthCodeLogin({clientId: clientId})
    console.log("Handle the response", resGoogle)

    // const backendUrl = `http://${window.location.hostname}/api/AuthGoogle/AuthCodeLogin`
    // const backendUrl = '/api/AuthGoogle/AuthCodeLogin'
    const backendUrl = `${import.meta.env.VITE_BACKEND_URI || ''}/api/AuthGoogle/AuthCodeLogin`
    console.log(`fetch URL: ${backendUrl}`)
    const response = await fetch(backendUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(resGoogle)
    })

    if (response.ok) {
        const data = await response.json()
        console.log('Server response:', data)    
        onResult(data)
    }
    else {
        onResult(new UserProfileResponse())
    }
}
