export class UserProfile {
    name: string | null = null
    email: string | null = null
    picture: string | null = null;
  }
  