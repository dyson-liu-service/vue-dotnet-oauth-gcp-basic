
export class ClientInfoResponse {
    result: boolean = false;
    message: string | null = null;
    data: { 
        "client_id": string | null 
    } = { "client_id": null };
}

export class UserProfileResponse {
    result: boolean = false;
    message: string | null = null;
    data: { 
        "provider": string | null,
        "sub": string | null,
        "email": string | null,
        "name": string | null,
        "picture": string | null
    } = { 
        "provider": null,
        "sub": null,
        "email": null,
        "name": null,
        "picture": null
    };
}
