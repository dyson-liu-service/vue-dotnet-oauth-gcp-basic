// Composables
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'Home',
        component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
      },
      {
        path: '/login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue'),
      },
      {
        path: '/user',
        name: 'UserProfile',
        component: () => import(/* webpackChunkName: "user-profile" */ '@/views/UserProfile.vue'),
      },
      {
        path: '/failed',
        name: 'LoginFailed',
        component: () => import(/* webpackChunkName: "login-failed" */ '@/views/LoginFailed.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
