// Reference:
// https://ithelp.ithome.com.tw/articles/10304289
// yobaji/vue3-google-login: https://github.com/yobaji/vue3-google-login
// https://yobaji.github.io/vue3-google-login/
// https://developers.google.com/identity/gsi/web/guides/get-google-api-clientid?hl=en
// https://www.google.com/search?q=oauth+2.0+client+ids+tutorial&oq=OAuth+2.0+Client+IDs+tu&aqs=chrome.1.69i57j33i160l2.7645j0j15&sourceid=chrome&ie=UTF-8#fpstate=ive&vld=cid:0fcc652e,vid:ex3FW_40izU

import App from './App.vue'
import { createApp } from 'vue'
import { registerPlugins } from '@/plugins'
import router from './router'

createApp(App)
    .use(router)
    .use(registerPlugins)
    .mount('#app')

    